class HumanPlayer
  def initialize(name)
    @name = name
  end

  def get_play
    gets.chomp.split(",").map(&:to_i)
  end

  def prompt
    puts "Enter a position (row,col) (ex. 3,4)"
    print "---> "
  end
end
