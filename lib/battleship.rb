class BattleshipGame
  attr_reader :board, :player

  def initialize(player = HumanPlayer.new("Allan"), board = Board.random)
    @player = player
    @board = board
    @hit = false
  end

  def attack(pos)
    if board[pos] == :s
      @hit = true
      board[pos] = :sunken_ship
    else
      @hit = false
      board[pos] = :x
    end
  end

  def count
    board.count
  end

  def conclude
    puts "Congratulations. You win!"
  end

  def confirm_hit
    board.display
    puts "It's a successful hit!" if hit?
    puts "#{count} ship(s) remaining."
  end

  def game_over?
    board.won?
  end

  def hit?
    @hit
  end

  def play
    until game_over?
      play_turn
    end
    conclude
  end

  def play_turn
    pos = nil
    pos = player.get_play
    attack(pos)
  end
end
